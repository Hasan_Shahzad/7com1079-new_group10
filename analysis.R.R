library(tidyverse) #including libraries
ds<- read_csv("dataset_newgroup10") #loading dataset
ds$alc<-rowMeans(subset(ds, select = c(Dalc, Walc)), na.rm = TRUE)
ds$alc<-round(ds$alc, 0)
#Computing count of students from rural and urban schools
a <- aggregate(ds$alc, list(ds$address), FUN=length)

#__________________Plotting Bar Chart____________________________________
barplot(
                 a$x, 
                 names.arg=c("Rural","Urban"),
                 main = "Consumption of alchohol among the students in the rural and the urban schools", 
                 xlab="Seconary School Students", 
                 ylab="Number of Students",
                 col = c("lightblue", "mistyrose"), 
                 args.legend = list(x ="topleft"),
                 legend.text=c("Rural","Urban")
        )
 
#__________________Plotting Histogram____________________________________
y <- ds$alc 
h <- hist(y,6
          
          
          ,
main = "Secondary School Students Alchohol Consumption",
xlab = "Alchohol Cosumption on a scale(1-5)",
ylab = "No. of Students",
col="mistyrose"
)
m <- mean(y) 
std <- sd(y) 
x <- seq(1,5,0.1)
y1 <- dnorm(x, mean=m, sd=std) 
y1 <- y1 * diff(h$mids[1:2])*length(y) 
lines(x, y1, col="blue")
