Group: new_group10 

Question
========

RQ: Is there a difference in the mean of alcohol consumption between urban secondary school students and rural secondary school students?

Null hypothesis: There is no difference in the mean of alcohol consumption between urban secondary school students and rural secondary school students.

Alternative hypothesis: There is difference in the mean of alcohol consumption between urban secondary school students and rural secondary school students.


Dataset
=======

URL: https://www.kaggle.com/uciml/student-alcohol-consumption

Column Headings: 

```

> grades <- read.csv("dataset_groupnew10.csv")
> colnames(grades)
 [1] "school"     "sex"        "age"        "address"    "famsize"
 [6] "Pstatus"    "Medu"       "Fedu"       "Mjob"       "Fjob"
[11] "reason"     "guardian"   "traveltime" "studytime"  "failures"
[16] "schoolsup"  "famsup"     "paid"       "activities" "nursery"
[21] "higher"     "internet"   "romantic"   "famrel"     "freetime"
[26] "goout"      "Dalc"       "Walc"       "health"     "absences"
[31] "G1"         "G2"         "G3"

```
